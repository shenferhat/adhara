pragma solidity ^0.4.0;

import './TokenStorage.sol';
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/lifecycle/Pausable.sol";

contract TokenRegistry is TokenStorage, Ownable, Pausable {
    address public logic_contract;

    function setLogicContract(address _c) public onlyOwner whenNotPaused returns (bool success){
        logic_contract = _c;
        return true;
    }
    function getLogicContract() public view returns (address) {
        return logic_contract;
    }

    function () payable public {
        address target = logic_contract;
        assembly {
            let ptr := mload(0x40)
            calldatacopy(ptr, 0, calldatasize)
            let result := delegatecall(gas, target, ptr, calldatasize, 0, 0)
            let size := returndatasize
            returndatacopy(ptr, 0, size)
            switch result
            case 0 { revert(ptr, size) }
            case 1 { return(ptr, size) }
        }
    }
}
