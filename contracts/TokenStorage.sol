pragma solidity ^0.4.0;

import "openzeppelin-solidity/contracts/token/ERC20/StandardToken.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";

contract TokenStorage {

    mapping(bytes32 => address) public tokens;
}
