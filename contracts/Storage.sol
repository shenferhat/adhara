pragma solidity ^0.4.21;

contract Storage {
    uint public val;

    function getVal() public view returns(uint) {
        return val;
    }
}