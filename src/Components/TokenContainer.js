import React, { Component } from 'react';

class TokenContainer extends Component {
    render(){
        return (
            <div className="container is-fullhd">
                <div className="notification">
                    This container is <strong>fullwidth</strong> <em>until</em> the <code>$fullhd</code> breakpoint.

                    <div className="field is-grouped">
                        <div className="control">
                            <button className="button is-link">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default TokenContainer;