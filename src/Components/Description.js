import React from 'react';

function Description(props) {
    return (
        <section className="container">
            <div className="has-text-centered content">
                <br/>
                <h1 className="title is-4 is-uppercase has-text-danger">Adhara</h1>
                <h2 className="subtitle is-6 has-text-grey-light">Token Controller</h2>
            </div>
        </section>
    )
}

export default Description;
    