import TruffleContract from 'truffle-contract'
import React, { Component } from 'react';
import Web3 from 'web3'
import Nav from './Components/Nav';
import Description from './Components/Description';

/*
import InstallMetamask from './Components/InstallMetamask';
import UnlockMetamask from './Components/UnlockMetamask';
import TokenContainer from './Components/TokenContainer';
 */

import Example from './build/Example.json';

class App extends Component {
    constructor(){
        super();
        this.appName = 'ATC';
        this.isWeb3 = true;                 //If metamask is installed
        this.isWeb3Locked = false;          //If metamask account is locked

        this.state = {
            tzAddress: null,
            inProgress: false,
            tx: null,
            network: 'Checking...',
            account: null
        };

        let web3 = window.web3;

        if (typeof web3 !== 'undefined') {
            // Use Mist/MetaMask's provider
            this.web3Provider = web3.currentProvider;
            this.web3 = new Web3(web3.currentProvider);

            // this.tokenZendr = TruffleContract(TokenZendR);
            // this.tokenZendr.setProvider(this.web3Provider);

            if (web3.eth.coinbase === null) this.isWeb3Locked = true;

        }else{
            this.isWeb3 = false;
        }
        /*
        super();
        this.appName = 'ATC';
        this.isWeb3 = true;                 //If metamask is installed
        this.isWeb3Locked = false;          //If metamask account is locked

        this.state = {
            tzAddress: null,
            inProgress: false,
            tx: null,
            network: 'Checking...',
            account: null,
            containerMessage:"Container Message",
            inputBox:"inputBox"
        };

        web3 = window.web3;

        if (typeof web3 !== 'undefined') {
            // Use Mist/MetaMask's provider
            this.web3Provider = web3.currentProvider;
            this.web3 = new Web3(web3.currentProvider);
            this.web3.eth.defaultAccount = web3.eth.accounts[0];

           // this.tokenZendr = TruffleContract(TokenZendR);
           // this.tokenZendr.setProvider(this.web3Provider);
            this.example = TruffleContract(Example);
            this.example.setProvider(this.web3Provider);

            if (web3.eth.coinbase === null) this.isWeb3Locked = true;

        }else{
            this.isWeb3 = false;
        }
        */

    }

    setNetwork = () => {
        let networkName,that = this;

        this.web3.version.getNetwork(function (err, networkId) {
            switch (networkId) {
                case "1":
                    networkName = "Main";
                    break;
                case "2":
                    networkName = "Morden";
                    break;
                case "3":
                    networkName = "Ropsten";
                    break;
                case "4":
                    networkName = "Rinkeby";
                    break;
                case "42":
                    networkName = "Kovan";
                    break;
                default:
                    networkName = networkId;
            }

            that.setState({
                network: networkName
            })
        });
    };

    componentDidMount(){
        if(this.isWeb3) {
            if(this.isWeb3Locked) {
                let account = this.web3.eth.coinbase;
                let app = this;

                this.setState({
                    account
                });
            }
        }

        this.setNetwork(this.state.network);
    }

    handleSubmit() {
        /*
        this.example.deployed().then( (instance) => {

            console.log("button click");

            console.log("deployeds");
            this.setString(instance);

        });

    }

    async setString(instance) {
        this.exampleInstance = instance;

        this.exampleInstance.setString(this.state.inputBox);
        */
    }



    handleSecond() {
        /*
        console.log("button click 2");
        this.example.deployed().then( (instance) => {

            console.log("deployeds 2");
            this.exampleInstance = instance;

            this.exampleInstance.getString().then((result) => {
                console.log(result);
            })
            this.setState({
                containerMessage:this.state.inputBox
            })

        });*/
    }

    render() {
        return(
            <div>
                <Nav appName={this.appName} network={this.state.network} />
                <Description />

                <div className="container is-fullhd">
                    <div className="notification">
                        {this.state.containerMessage}

                        <div className="field">
                            <label className="label">Label</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="Text input"
                                       onChange={(e) => {this.setState({ inputBox:e.target.value }) }} />
                            </div>
                            <p className="help">This is a help text</p>
                        </div>

                        <div className="field is-grouped">
                            <div className="control">
                                <button className="button is-link" onClick={this.handleSubmit.bind(this)}>Set</button>
                            </div>

                            <div className="control">
                                <button className="button is-link" onClick={this.handleSecond.bind(this)}>Get</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        )
    /*
        if(this.isWeb3) {
            if(this.isWeb3Locked) {
                return (
                    <div>
                        <Nav appName={this.appName} network={this.state.network} />
                        <UnlockMetamask message="Unlock Your Metamask/Mist Wallet" />
                    </div>
                )
            }else {
                return (
                    <div>
                        <Nav appName={this.appName} network={this.state.network} />
                        <Description />

                    </div>
                )
            }
        }else{
            return(
                <InstallMetamask />
            )
        }
    */
    }
}

export default App;