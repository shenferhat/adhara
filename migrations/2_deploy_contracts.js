var TokenControl = artifacts.require("./TokenControl.sol");
var Registry = artifacts.require("./Registry.sol")
var LogicOne = artifacts.require("./LogicOne.sol")
var LogicTwo = artifacts.require("./LogicTwo.sol")
var Storage = artifacts.require("./Storage.sol")

module.exports = function(deployer) {
    //deployer.deploy(TokenControl);
    deployer.deploy(Registry);
    deployer.deploy(LogicOne);
    deployer.deploy(LogicTwo);
    deployer.deploy(Storage);
};
