const Registry = artifacts.require('./Registry.sol');
const LogicOne = artifacts.require('./LogicOne.sol');
const LogicTwo = artifacts.require('./LogicTwo.sol');
const Storage = artifacts.require('./Storage.sol');


const should = require('chai')
    .use(require('chai-as-promised'))
    .should();

let logicone, registry,logictwo,storage;

var BigNumber = require('bignumber.js');
const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'))
var util = require('web3-utils');

contract('Proxy Logic Test', async ([owner, ...accounts]) => {
    let accountA, accountB, accountC, accountD;
    [accountA, accountB, accountC, accountD ] = accounts;

    beforeEach(async () => {
        registry = await Registry.new();
        logicone = await LogicOne.new();
        logictwo = await LogicTwo.new();
        storage = await Storage.new();
    });


    it("proxy logic", async() => {



        await registry.setLogicContract(logicone.address);

        let logic_address = await registry.getLogicContract();
        console.log(logic_address);

        await registry.setLogicContract(logictwo.address);

        logic_address = await registry.getLogicContract();
        console.log(logic_address);

        /*
        await logicone.setVal(3);
        console.log("amk1");
        let ret2 = await logicone.getVal();
        console.log(ret2.toNumber());
        console.log("amk end1");

        await registry.setLogicContract(logictwo.address);

        await logictwo.setVal(4);
        console.log("amk2");
        let ret1 = await logictwo.getVal();
        console.log(ret1.toNumber());
        console.log("amk end2");

        Registry.at(Registry.address).setLogicContract(LogicTwo.address)

        */
        //Registry.at(Registry.address).setLogicContract(LogicOne.address)
        //LogicOne.at(Registry.address).setVal(2)
        //Registry.at(Registry.address).setLogicContract(LogicTwo.address)
        //LogicTwo.at(Registry.address).setVal(2)
    });


});