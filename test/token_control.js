const ShenToken = artifacts.require('./ShenToken.sol');
const FerhatToken = artifacts.require('./FerhatToken.sol');
const TokenControl = artifacts.require('./TokenControl.sol');

const should = require('chai')
    .use(require('chai-as-promised'))
    .should();

let sender, shentoken, ferhattoken;

var BigNumber = require('bignumber.js');
const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'))
var util = require('web3-utils');

contract('Token Controller', async ([owner, ...accounts]) => {
    let accountA, accountB, accountC, accountD;
    [accountA, accountB, accountC, accountD ] = accounts;

    beforeEach(async () => {
        shentoken = await ShenToken.new();
        ferhattoken = await FerhatToken.new();
        sender = await TokenControl.new();

        await sender.addNewToken(util.utf8ToHex("SEN"), shentoken.address);
        await sender.addNewToken(util.utf8ToHex("FRHT"), ferhattoken.address);
    });

    it("should send token between addresses", async() => {
        let returnStr = await sender.getOwner();

        console.log(returnStr);

        console.log(accountA);
        console.log(accountB);
        console.log(accountC);
        console.log(accountD);


        // transferTokens(bytes32 symbol_, address to_, uint256 amount_)
    });

    it("should update supported token address", async() => {
        await sender.addNewToken(util.utf8ToHex('SEN'), accountA);

        let address3 = await sender.tokens.call(util.utf8ToHex("SEN"));

        address3.should.equal(accountA);
    });

    it("should be able to transfer sender token to another wallet", async() => {
        let amount = new BigNumber(500000e5);

        //Account a approve contract to spend on behalf
        await shentoken.approve(sender.address, amount,{from: owner});

        await sender.transferTokens(util.utf8ToHex("SEN"),accountB, amount,{from: owner});

        let balance = ((await shentoken.balanceOf(accountB)).toString());

        balance.should.equal(amount.toString())
    });



});